import {Pipe, PipeTransform} from '@angular/core';
import {User} from "../models/user/user";


@Pipe({
    name: 'searchByName'
})

export class SearchByNamePipe implements PipeTransform{
    transform(usersList: User[], conditions: string) {
        return usersList.filter(user => this.getAll(user).toLowerCase().indexOf(conditions.toLowerCase()) !== -1);
    }

    getAll(user:User):string {
        return user.firstName + user.lastName + user.mail;
    }
}