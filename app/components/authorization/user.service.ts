import {Injectable, OnInit} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { CoolLocalStorage } from 'angular2-cool-storage';
import {Constants} from "../../consts";

@Injectable()
export class UserService implements OnInit{
    private loggedIn = false;
    localStorage: CoolLocalStorage;

    constructor(localStorage: CoolLocalStorage,
                private http: Http) {
        this.loggedIn = !!localStorage.getItem('auth_token');
    }

    ngOnInit() {
        this.localStorage.setItem('itemKey', 'itemValue');
        this.localStorage.setObject('itemKey', {
            someObject: 3
        });
    }


    login(user) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let body = JSON.stringify({ user });
        console.log(body);
        let url = Constants.BASE_URL + '/login';

        return this.http.post(url, body, {headers: headers})
            .map(res => res.json())
            .map((res) => {
                        if (res.success) {
                            localStorage.setItem('auth_token', res.user.token);
                            localStorage.setItem('auth_id', res.user._id);
                            localStorage.setItem('firstName', res.user.firstName);
                            localStorage.setItem('lastName', res.user.lastName);
                            localStorage.setItem('mail', res.user.mail);
                            localStorage.setItem('photo', res.user.photo);
                            this.loggedIn = true;
                        }
                        console.log(res);
                        return res;
                    });
    }

    logout() {
        localStorage.removeItem('auth_token');
        localStorage.removeItem('auth_id');
        localStorage.removeItem('firstName');
        localStorage.removeItem('lastName');
        localStorage.removeItem('mail');
        localStorage.removeItem('photo');
        this.loggedIn = false;
    }

    isLoggedIn() {
        return this.loggedIn;
    }
}