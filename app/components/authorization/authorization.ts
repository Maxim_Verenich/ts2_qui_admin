import {Component} from '@angular/core';
import { Router } from '@angular/router';
import {Footer} from "../ts-footer/ts-footer";

import { UserService } from './user.service';
import {Constants} from "../../consts";

@Component({
    selector: 'authorization',
    templateUrl: './app/components/authorization/authorization.html',
    directives: [Footer]
})

export class Authorization {
    errMessage:string;

    constructor(private userService: UserService, private router: Router) {}

    onSubmit(user) {
        this.userService.login(user).subscribe((result) => {
            if (result) {
                console.log('role ', result.user.role);
                if(result.user.role == Constants.ADMIN_ROLE) {
                    this.router.navigate(['/home']);
                } else {
                    this.errMessage = 'Incorrect login or password';
                    console.log(this.errMessage);
                }
            }
        });
    }
}
