import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, NavigationStart, Event} from '@angular/router';
import {UserService} from "../authorization/user.service";

@Component({
    selector: 'ts-header',
    templateUrl: './app/components/ts-header/ts-header.html',
    directives: [ROUTER_DIRECTIVES]
})

export class Header implements OnInit {
    backgroundList = ['img/rx-map.jpg', 'img/sea.jpg', 'img/fontY7.jpg', 'img/fontY9.jpg', 'img/fontY6.jpg', 'img/Италия.jpg'];

    constructor(private router:Router,
                private userService:UserService) {
        // router.events.subscribe((event:Event) => {
        // })
    }

    ngOnInit() {
        document.body.style.background = 'url(' + localStorage.getItem('background1') + ')' + 'no-repeat center center fixed';
        document.body.style.webkitBackgroundColor = 'cover';
        document.body.style.backgroundSize = 'cover';
    }

    logout() {
        event.preventDefault();
        this.userService.logout();
        this.router.navigate(['']);
    }

    background:string;

    selectBackground(background) {
        event.preventDefault();
        document.body.style.background = 'url(' + background + ')' + 'no-repeat center center fixed';
        document.body.style.webkitBackgroundColor = 'cover';
        document.body.style.backgroundSize = 'cover';
        localStorage.setItem('background1', background);
    }
}


