import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {Footer} from '../ts-footer/ts-footer';
import {Header} from '../ts-header/ts-header';
import {Constants} from '../../consts';
import {UserDataService} from "../../models/user/user-data.service";
import {PickerUtil} from "../../utils/picker.util";
import {User} from "../../models/user/user";
import {Notify} from "../../models/notify/notify";
import {NotifyService} from "../../models/notify/notify.service";
import {TestsService} from "../../models/statistics/test-data.service";


//noinspection TypeScriptValidateTypes
@Component({
    selector: 'home',
    templateUrl: './app/components/admin-home/admin-home.html',
    directives: [ROUTER_DIRECTIVES, Footer, Header]
})

export class Home implements OnInit {

    teachersList = [
        {
            'id': '578e18be963494c000ffd66b',
            'firstName': 'Yulia',
            'lastName': 'Galuzo',
            'role': 1,
            'mail': 'yulia.galuzo@gmail.com',
            'photo': './img/avatar1.png'
        },
        {
            'id': '578e18be963494c000ffd65b',
            'firstName': '1234terg',
            'lastName': 'gfcjvhj',
            'role': 2,
            'mail': 'yulia.galuzo9771@gmail.com',
            'photo': './img/avatar2.png'
        },
        {
            'id': '578e18be963494c000ffd36b',
            'firstName': 'Mary',
            'lastName': 'Galuzo',
            'role': 2,
            'mail': 'yulia.galuzo15@gmail.com',
            'photo': './img/avatar3.png'
        },
        {
            'id': '572e18be963494c000ffd66b',
            'firstName': 'Mary',
            'lastName': 'Galuzo',
            'role': 2,
            'mail': 'yulia.galuzo15@gmail.com',
            'photo': './img/avatar3.png'
        },
        {
            'id': '578e18be963494c050ffd66b',
            'firstName': 'Mary',
            'lastName': 'Galuzo',
            'role': 2,
            'mail': 'yulia.galuzo15@gmail.com',
            'photo': './img/avatar3.png'
        }
    ];

    messages:Array<Notify>;
    siteStat:Array<any>;
    e;

    usersList:User[];
    searchValue:string;
    assignTest:any;
    USER_ASSIGN_TEST:string = '0';
    USER_PASSED_TEST:string = '1';
    TEACHER_CHECK_TEST:string = '2';
    saveUserId:any;

    constructor(private router:Router,
                private notifyService:NotifyService,
                private userDataService:UserDataService,
                private testsService: TestsService,
                private pickerUtl:PickerUtil) {
        this.usersList = [];
        this.searchValue = '';
        this.assignTest = {
            startTime: '',
            finishTime: '',
            duration: '',
            candidateId: ''
        };
        this.messages = [];
        this.siteStat = [{userCount: '', teacherCount: ''}, {checkedTestCount: '', passedTestCount: ''}];
    }

    getNotify() {
        this.notifyService.getNotify()
            .subscribe(messages => {
                this.messages = messages;
                this.messages.reverse();
                console.log(messages);
            });
    }


    getSiteStatistic(){
        this.testsService.getSiteStatistic()
            .subscribe(siteStat =>{
                this.siteStat = siteStat;
                console.log(this.siteStat);
            });
    }

    ngOnInit(){
        this.getSiteStatistic();
        this.getNotify();
    }

    showAnimation(event, user) {
        this.saveUserId = user;
        event.preventDefault();
        this.e = event;
        var elem = document.querySelector('div.assignTestFromStartPage.assignTest');
        var elemBack = document.querySelector('div.my-div.test-anim-home');
        // var card = document.getElementsByClassName('card-panel')[0]
        var card = this.searchParent(this.e.target);
        elem = document.querySelector('div.assignTestFromStartPage');
        elem.classList.add('assignTest');
        elemBack = document.querySelector('div.my-div');
        elem.classList.add('assignTest');
        elemBack.classList.add('test-anim-home');
        card.classList.add('fake');

    }

    brakeAnimation(event) {
        event.preventDefault();
        var elem = document.querySelector('div.assignTestFromStartPage.assignTest');
        var elemBack = document.querySelector('div.my-div.test-anim-home');
        var card = this.searchParent(this.e.target);
        elem.classList.remove('assignTest');
        elemBack.classList.remove('test-anim-home');
        card.classList.remove('fake');
    }

    showAnimation1(event) {
        event.preventDefault();
        this.e = event;
        var elem = document.querySelector('div.assignTestFromStartPage1.tsY-user1');
        var elemBack = document.querySelector('div.my-div.test-anim1');
        var card = this.searchParent(this.e.target);
        elem = document.querySelector('div.assignTestFromStartPage1');
        elem.classList.add('tsY-user1');
        elemBack = document.querySelector('div.my-div');
        elem.classList.add('tsY-user1');
        elemBack.classList.add('test-anim1');
        card.classList.add('fake');

    }

    brakeAnimation1(event) {
        event.preventDefault();
        var elem = document.querySelector('div.assignTestFromStartPage1.tsY-user1');
        var elemBack = document.querySelector('div.my-div.test-anim1');
        var card = this.searchParent(this.e.target);
        elem.classList.remove('tsY-user1');
        elemBack.classList.remove('test-anim1');
        card.classList.remove('fake');
    }

    searchParent(elem) {
        while (elem != this) {
            if (elem.classList.contains('ts-home-card')) {
                return elem;
            }
            elem = elem.parentNode;
        }
    }

    deleteNotification(i:number) {
        event.preventDefault();
        this.notifyService.deleteNotify(this.messages[i])
            .subscribe(
                data => {console.log('Notify' + this.assignTest.candidateId + ' has been deleted!'); this.messages.splice(i, 1);},
                error => console.log(error)
            );
    }

    openPicker(event, attr) {
        this.pickerUtl.openPicker(event, this.assignTest, attr, {});
        document.getElementsByClassName('c-datepicker')[0].classList.add('some')
    }

    onAssignTest(candidateId: string) {
        event.preventDefault();
        this.assignTest.candidateId = candidateId;
        this.userDataService.assignTest(this.assignTest)
            .subscribe(
                data => {console.log('Test for user ' + this.assignTest.candidateId + ' has been created!');
                console.log(data);},
                error => console.log(error.json().message)
            );
        this.brakeAnimation(event);
    }

    goToStatistics(parentId:string) {
        event.preventDefault();
        this.router.navigate(['userDetail', parentId, 0]);
    }
}

