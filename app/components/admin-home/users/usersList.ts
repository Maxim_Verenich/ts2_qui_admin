import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Header} from "../../ts-header/ts-header";
import {Footer} from "../../ts-footer/ts-footer";
import {SearchByNamePipe} from "../../../pipes/search.name.pipe";
import {UserDataService} from "../../../models/user/user-data.service";
import {User} from "../../../models/user/user";
import {FORM_DIRECTIVES} from '@angular/common';
import {PickerUtil} from "../../../utils/picker.util";

// declare function MaterialDatePicker({}): void;

@Component({
    selector: 'users-list',
    templateUrl: './app/components/admin-home/users/usersList.html',
    directives: [Footer, Header, FORM_DIRECTIVES],
    pipes: [SearchByNamePipe]
})

export class UsersList {
    usersList:User[];
    searchValue:string;
    assignTest:any;

    constructor(private router:Router,
                private userDataService:UserDataService,
                private pickerUtl:PickerUtil) {
        this.usersList = [];
        this.searchValue = '';
        this.assignTest = {
            startTime: '',
            finishTime: '',
            duration: '',
            candidateId: ''
        };
    }

    getUsers() {
        this.userDataService.getUsersList(0)
            .subscribe(
                users => {
                    this.usersList = users;
                    console.log(this.usersList[0]);
                    console.log(this.usersList[0]._id);
                });
        // error =>  this.errorMessage = <any>error);
    }

    ngOnInit() {
        this.getUsers();
    }

    onAssignTest(candidateId: string) {
        
        this.assignTest.candidateId = candidateId;
        this.userDataService.assignTest(this.assignTest)
            .subscribe(
                data => console.log('Test for user ' + this.assignTest.candidateId + ' has been created!'),
                error => console.log(error.json().message)
            );
    }

    addGuest(guest:User) {
        this.userDataService.addGuest(guest)
            .subscribe(
                data => console.log('Your account has been created!'),
                error => console.log(error.json().message)
            );
        guest.firstName='';
        guest.lastName='';
        guest.mail='';
    }

    gotoDetail(parentId:string) {
        event.preventDefault();
        console.log('user id ', parentId);
        var id = 0;
        this.router.navigate(['userDetail', parentId, id]);
    }

    openPicker(event, attr) {
        this.pickerUtl.openPicker(event, this.assignTest, attr);
    }
}

