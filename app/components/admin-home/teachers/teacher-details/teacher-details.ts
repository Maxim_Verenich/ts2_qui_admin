import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {Footer} from "../../../ts-footer/ts-footer";
import {Header} from "../../../ts-header/ts-header";
import {Test} from "./test";
import {TestDataService} from "../../../../models/test/test-data.service";
import {UserDataService} from "../../../../models/user/user-data.service";
import {User} from "../../../../models/user/user";
import {PickerUtil} from "../../../../utils/picker.util";

@Component({
    selector: 'teacher-charts',
    templateUrl: './app/components/admin-home/teachers/teacher-details/teacher-details.html',
    directives: [ROUTER_DIRECTIVES, Footer, Header]
})

export class TeacherDetailsComponent implements OnInit, OnDestroy {
    teacher:User;
    testsList:Test[];
    sub:any;
    testsForCheck:any;
    assignTest:any;

    constructor(private teacherService:UserDataService,
                private testDataService:TestDataService,
                private route:ActivatedRoute,
                private pickerUtl:PickerUtil) {
        this.teacher = new User();
        this.testsForCheck = new Array;
        this.assignTest = {
            startTime: '',
            finishTime: '',
            duration: '',
            candidateId: ''
        };
    }

    getTests() {
        this.testDataService.getTests().subscribe(tests => {
            console.log(tests);
            this.testsList = tests;
        });
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let id = params['id'];
            this.teacherService.getUserById(id).subscribe(user => this.teacher = user);
            console.log(this.teacher);
        });
        this.getTests();
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }

    postSendTestToTeacherForCheck() {
        var test = [];
        for (var i = 0; i < this.testsForCheck.length; i++) {
            if (this.testsForCheck[i] == true) {
                test.push(this.testsList[i]._id);
            }
        }

        window.history.back();

        this.teacherService.sendTestsForTeacher(test, this.teacher)
            .subscribe(
                data => console.log('Your tests has been send to teacher!'),
                error => alert(error)
            );
    }

    openPicker(event, attr) {
        this.pickerUtl.openPicker(event, this.assignTest, attr, {});
        document.getElementsByClassName('c-datepicker')[0].classList.add('assign')
    }
}
