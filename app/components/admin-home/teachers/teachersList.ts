import {Component} from '@angular/core';
import {Header} from "../../ts-header/ts-header";
import {Footer} from "../../ts-footer/ts-footer";
import {Router} from "@angular/router";
import {UserDataService} from "../../../models/user/user-data.service";
import {User} from "../../../models/user/user";
import {SearchByNamePipe} from "../../../pipes/search.name.pipe";

@Component({
    selector: 'teachers-list',
    templateUrl: './app/components/admin-home/teachers/teachersList.html',
    directives: [ Footer, Header],
    pipes: [SearchByNamePipe]
})

export class TeachersList {
    teachersList : User[];
    searchValue:string;
    errorMessage:string;

    constructor(private router: Router,
        private userDataService : UserDataService){
        this.teachersList = [];
        this.searchValue = '';
    }

    getUsers() {
        this.userDataService.getUsersList(1)
            .subscribe(
                users => this.teachersList = users,
                error =>  this.errorMessage = <any>error);
    }

    ngOnInit(){
        this.getUsers();
    }

    gotoDetail(id:string) {
        event.preventDefault();
        this.router.navigate(['teacherDetail', id]);
    }

    // onAssignTest(id:string){
    //     event.preventDefault();
    //     this.userDataService.assignTest(id)
    //         .subscribe(
    //             data => {
    //                 // alert('You assigned test!');
    //                 this.router.navigate(['teacherDetail', id]);
    //             },
    //             error =>  this.errorMessage = <any>error
    //         );
    // }
    //
}


