import {Component, Inject, OnInit, ElementRef} from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgStyle} from '@angular/common';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {Header} from "../../ts-header/ts-header";
import {Footer} from "../../ts-footer/ts-footer";
import { Question } from "../../../models/test/question/question";
import { TestDataService } from "../../../models/test/test-data.service";
import { Constants } from "../../../consts";
import {Reading} from "../../../models/test/reading/reading";
import {Listening} from "../../../models/test/listening/listening";
import {FILE_UPLOAD_DIRECTIVES, FileUploader} from 'ng2-file-upload';
import {HTTP_PROVIDERS} from '@angular/http';

@Component({
    selector: 'ts-test',
    templateUrl: './app/components/admin-home/test/test.html',
    directives: [ROUTER_DIRECTIVES, Footer, Header, FILE_UPLOAD_DIRECTIVES, NgClass, NgStyle, CORE_DIRECTIVES, FORM_DIRECTIVES],
    providers: [HTTP_PROVIDERS]
})

export class Test implements OnInit{

    ngOnInit() {
         this.uploader = new FileUploader({url: 'http://localhost:8083/api/tasks/sendAudio', withCredentials: false});
    }

    uploadFile() {
        console.log("success", this.uploader.queue[0]);
        let file = this.uploader.queue[0];
        this.fileName = file.file.name;
        file.upload();
    }

    fileName: string;
    URL:string = 'http://localhost:8083/api/tasks/sendAudio';
    uploader: FileUploader;
    hasBaseDropZoneOver:boolean = false;
    hasAnotherDropZoneOver:boolean = false;

    fileOverBase(e:any):void {
        this.hasBaseDropZoneOver = e;
    }

    fileOverAnother(e:any):void {
        this.hasAnotherDropZoneOver = e;
    }

    s:number = 1;
    addLGT: Array<Question>;
    addSpeaking: Array<Question>;
    addReading: any;
    addListening:any;
    levelList = ['A1', 'A2', 'B1', 'B2', 'C1'];
    items = [1, 2, 3, 4, 5];
    variantOfAnswer = ['a', 'b', 'c'];

    constructor(private testDataService: TestDataService) {
        this.addLGT = new Array;
        this.addSpeaking = new Array;
        this.addReading = new Reading();
        this.addListening = new Listening();
        var question = new Question(3);
        var question1 = new Question(0);
        this.addLGT.push(question);
        this.addSpeaking.push(question1);
    }

    section(num:number) {
        this.s = num;
    }

    selectLevelForLexicalGrammarTest(level:string, i:number) {
        event.preventDefault();
        this.addLGT[i].level = level;
    }
    selectLevelForReading(level:string) {
        event.preventDefault();
        this.addReading.text.level = level;
    }
    selectLevelForListening(level:string) {
        event.preventDefault();
        this.addListening.level = level;
    }
    selectLevelForSpeaking(level:string, i:number) {
        event.preventDefault();
        this.addSpeaking[i].level = level;
    }

    postQuestionLGT() {
        var test = this.addLGT;
        this.testDataService.addQuestionForLGT(test, Constants.LEXICAL_GRAMMAR_ID)
            .subscribe(
                data => console.log('Your question has been created!'),
                error => alert(error.json().message)
            );
    }
    postQuestionReading(){
        var test = this.addReading;
        console.log(test.statementTask);
        this.testDataService.addTask(test, Constants.READING_ID)
            .subscribe(
                data => console.log('Your question has been created!'),
                error => alert(error.json().message)
            );
    }
    postQuestionListening(){
        var test = this.addListening;
        this.addListening.text.description = this.fileName;
        this.addListening.text.title = 'Listen a story';
        this.addListening.text.level = 'B1';
        this.addListening.questionTask.title = 'Answer the question';
        this.addListening.completeTheSentencesTask.title = 'Complete the sentences';
        this.testDataService.addTask(test, Constants.LISTENING_ID)
            .subscribe(
                data => console.log('Your question has been created!'),
                error => alert(error.json().message)
            );
    }
    postQuestionSpeaking() {
        var test = this.addSpeaking;
        console.log(test);
        this.testDataService.addQuestionForSpeaking(test, Constants.SPEAKING_ID)
            .subscribe(
                data => console.log('Your question has been created!'),
                error => alert(error.json().message)
            );
    }

    selectTextForStatementTaskQuestion(i: number){
        this.addReading.statementTask.questions[i].answersId[0].text = "true";
        this.addReading.statementTask.questions[i].answersId[1].text = "false";
    }
    addQuestionForLGT() {
        var question = new Question(3);
        this.addLGT.push(question);
    }
    addStatementForReading(){
        var statement = new Question(2);
        this.addReading.statementTask.questions.push(statement);
    }
    addQuestionForReading(){
        var statement = new Question(3);
        this.addReading.translationTask.questions.push(statement);
    }
    addQuestionForListening(){
        var question = new Question(0);
        this.addListening.questionTask.questions.push(question);
    }
    addTaskQuestionForListening(){
        //this.audio.queue[0].upload;
        var question = new Question(3);
        this.addListening.completeTheSentencesTask.questions.push(question);
    }
    addQuestionForSpeaking(){
        var question = new Question(0);
        this.addSpeaking.push(question);
    }
    deleteQuestion(questions, i){
        questions.splice(i, 1);
    }
}

    

