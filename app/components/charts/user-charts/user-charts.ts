import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {Footer} from "../../ts-footer/ts-footer";
import {Header} from "../../ts-header/ts-header";
import {Router} from '@angular/router';
import {TestsService} from "../../../models/statistics/test-data.service";
import {Test} from "../../../models/statistics/test";
import {UserDataService} from "../../../models/user/user-data.service";
import {User} from "../../../models/user/user";
import {PickerUtil} from "../../../utils/picker.util";


declare var d3:any;

@Component({
    selector: 'user-charts',
    templateUrl: './app/components/charts/user-charts/user-charts.html',
    directives: [ROUTER_DIRECTIVES, Footer, Header]
})

export class UserChartComponent implements OnInit, OnDestroy {
    user:User;
    sub:any;
    testsList:any[];
    teachersList:User[];
    usersList:User[];
    test:any;
    assignTest:any;
    s:number = 0;
    searchValue:string;

    constructor(private testsService:TestsService,
                private userService:UserDataService,
                private route:ActivatedRoute,
                private router:Router,
                private userDataService:UserDataService,
                private pickerUtl:PickerUtil) {
        this.test = {
            'part1': [],
            'part2': [],
            'part3': [],
            'part4': [],
            'marks': [],
            'isChecked': false
        };
        this.testsList = [];
        this.user = new User();
        this.usersList = [];
        this.searchValue = '';
        this.assignTest = {
            startTime: '',
            finishTime: '',
            duration: '',
            candidateId: ''
        };
    }

    getTests(testId:number, userId:string) {
        this.testsService.getTestsList(userId).subscribe(tests => {
            this.testsList = tests;
            let test_Id = this.testsList[testId]._id;
            this.getStatistics(test_Id);
            (tests.length == 1) && (this.s = -1);
        });
    }

    getStatistics(id:string) {
        this.testsService.getTestById(id).subscribe(statistics => {
                this.test = new Test(statistics);
                this.dashboard();
            },
            err => console.error(err));
    }

    ngOnInit() {
        this.userService.getUsersList(1).subscribe(
            users => {
                this.teachersList = users;
            },
            error => console.log(error));
        this.sub = this.route.params.subscribe(params => {
            let id = params['id'];
            let userId = params['userId'];
            this.userService.getUserById(userId).subscribe(user => {
                this.user = user;
                this.getTests(id, userId);
            });
        });
    }

    section(num:number) {
        this.s = num;
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }


    onSelectTest(id:number) {
        event.preventDefault();
        d3.select("svg").remove();
        this.router.navigate(['userDetail/', this.user._id, id]);
    }

    onAssignTest(candidateId:string) {
        this.assignTest.candidateId = candidateId;
        this.userDataService.assignTest(this.assignTest)
            .subscribe(
                data => console.log('Test for user ' + this.assignTest.candidateId + ' has been created!'),
                error => console.log(error.json().message)
            );
        this.showAnimation(event, 'row');
    }

    showAnimation(event, mainParent) {
        event.preventDefault();
        var elem = document.querySelector('div.assignTestFromStartPage.tsY-user');
        var elemBack = document.querySelector('div.my-div.test-anim');
        var card = this.searchParent(event.target, mainParent);
        if (elem) {
            elem.classList.remove('tsY-user');
            elemBack.classList.remove('test-anim');
            card.classList.remove('fake');
        } else {
            elem = document.querySelector('div.assignTestFromStartPage');
            elemBack = document.querySelector('div.my-div');
            elem.classList.add('tsY-user');
            elemBack.classList.add('test-anim');
            card.classList.add('fake');
        }
    }

    showAnimation1(event, mainParent) {
        event.preventDefault();
        var elem = document.querySelector('div.assignTestFromStartPage1.tsY-user');
        var elemBack = document.querySelector('div.my-div.test-anim');
        var card = this.searchParent(event.target, mainParent);
        if (elem) {
            elem.classList.remove('tsY-user');
            elemBack.classList.remove('test-anim');
            card.classList.remove('fake');
        } else {
            elem = document.querySelector('div.assignTestFromStartPage1');
            elemBack = document.querySelector('div.my-div');
            elem.classList.add('tsY-user');
            elemBack.classList.add('test-anim');
            card.classList.add('fake');
        }
    }

    searchParent(elem, mainParent) {
        while (elem != this) {
            if (elem.classList.contains(mainParent)) {
                return elem;
            }
            elem = elem.parentNode;
        }
    }

    openPicker(event, attr) {
        this.pickerUtl.openPicker(event, this.assignTest, attr, {});
        document.getElementsByClassName('c-datepicker')[0].classList.add('assign')
    }


    dashboard() {

        var bubbleChart = new d3.svg.BubbleChart({
            supportResponsive: true,
            size: 600,
            innerRadius: 600 / 3.5,
            radiusMin: 50,
            data: {
                items: [
                    {text: "LGT", count: this.test.marks[0]},
                    {text: "reading", count: this.test.marks[1]},
                    {text: "listening", count: this.test.marks[2]},
                    {text: "speaking", count: this.test.marks[3]},
                    {text: "incorrect", count: "12"},
                ],
                eval: function (item) {
                    return item.count;
                },
                classed: function (item) {
                    return item.text.split(" ").join("");
                }
            },
            plugins: [
                {
                    name: "central-click",
                    options: {
                        style: {
                            "font-size": "12px",
                            "font-style": "italic",
                            "font-family": "Trajan Pro, sans-serif",
                            "text-anchor": "middle",
                            "fill": "white"
                        },
                        attr: {dy: "65px"}
                    }
                },
                {
                    name: "lines",
                    options: {
                        format: [
                            {// Line #0
                                textField: "count",
                                classed: {count: true},
                                style: {
                                    "font-size": "28px",
                                    "font-family": "Trajan Pro, sans-serif",
                                    "text-anchor": "middle",
                                    fill: "white"
                                },
                                attr: {
                                    dy: "0px",
                                    x: function (d) {
                                        return d.cx;
                                    },
                                    y: function (d) {
                                        return d.cy;
                                    }
                                }
                            },
                            {// Line #1
                                textField: "text",
                                classed: {text: true},
                                style: {
                                    "font-size": "14px",
                                    "font-family": "Trajan Pro, sans-serif",
                                    "text-anchor": "middle",
                                    fill: "white"
                                },
                                attr: {
                                    dy: "20px",
                                    x: function (d) {
                                        return d.cx;
                                    },
                                    y: function (d) {
                                        return d.cy;
                                    }
                                }
                            }
                        ],
                        centralFormat: [
                            {// Line #0
                                style: {"font-size": "80px"},
                                attr: {}
                            },
                            {// Line #1
                                style: {"font-size": "40px"},
                                attr: {dy: "40px"}
                            }
                        ]
                    }
                }]
        });
    }
}
