import {Injectable, OnInit} from '@angular/core';
import {Test} from "../../components/admin-home/teachers/teacher-details/test";
import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable }     from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import {CoolLocalStorage} from "angular2-cool-storage/index";
import {Constants} from "../../consts";
import {Notify} from "./notify";

@Injectable()
export class NotifyService implements OnInit{

    localStorage: CoolLocalStorage;

    constructor(localStorage: CoolLocalStorage,
                private http: Http) {
        this.localStorage = localStorage;
    }

    ngOnInit() {
        this.localStorage.setItem('itemKey', 'itemValue');

        console.log(this.localStorage.getItem('itemKey'));

        this.localStorage.setObject('itemKey', {
            someObject: 3
        });

        console.log(this.localStorage.getObject('itemKey'));
    }

    getNotify(): Observable<Notify[]>{
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        let url = Constants.BASE_URL + '/notifications/';
        return this.http.get(url, options)
            .map(response => response.json());
    }

    deleteNotify(notify: Notify): Observable<any>{
        let body = JSON.stringify(notify._id);
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(notify._id);
        console.log(body);
        let url = Constants.BASE_URL + '/notifications/' + notify._id;
        return this.http.delete(url, options);
    }
}