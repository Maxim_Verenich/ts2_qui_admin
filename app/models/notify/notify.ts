export class Notify{
    userId: string;
    type: string;
    userName:string;
    reviewerId:string;
    reviewerName:string;
    image: string;
    _id : string;
    
    constructor(){
        this.userId = '';
        this.type = '';
        this.userName = '';
        this.image = 'img/avatar3.png';
        this._id = '';
        this.reviewerId='';
        this.reviewerName='';
    }
}