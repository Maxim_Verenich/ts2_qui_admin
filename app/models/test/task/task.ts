import {Question} from "../question/question";

export class Task {
    title:string;
    questions:Array<Question>;

    constructor(questions:number, answers:number ) {
        this.title = '';
        this.questions = [];
        for (var i = 0; i < questions; i++) {
            this.questions.push(new Question(answers));
        }
    }
}
