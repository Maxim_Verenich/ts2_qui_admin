import {Injectable, OnInit} from '@angular/core';
import {Test} from "../../components/admin-home/teachers/teacher-details/test";
import { Question } from "./question/question";
import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable }     from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import {CoolLocalStorage} from "angular2-cool-storage/index";
import {Constants} from "../../consts";
import {Reading} from "./reading/reading";
import {Listening} from "./listening/listening";
// import {TESTSLIST} from "./mocks";

@Injectable()
export class TestDataService implements OnInit{

	localStorage: CoolLocalStorage;

    constructor(localStorage: CoolLocalStorage,
                private http: Http) {
        this.localStorage = localStorage;
    }

    ngOnInit() {
        this.localStorage.setItem('itemKey', 'itemValue');

        console.log(this.localStorage.getItem('itemKey'));

        this.localStorage.setObject('itemKey', {
            someObject: 3
        });

        console.log(this.localStorage.getObject('itemKey'));
    }

    getTests(): Observable<Test[]> {
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken});
        let options = new RequestOptions({ headers: headers });
        let url = Constants.BASE_URL + '/tests/isPassed';
        return this.http.get(url, options)
            .map(response => response.json());
    }

    addQuestion(question: Question, TOPIC_ID: string): Observable<Question>{
    	let body = JSON.stringify({ question });
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/tasks/' + TOPIC_ID;

        return this.http.post(url, body, options)
            .map(response => response.json());
    }
    
    addTask(task: Reading, TOPIC_ID:string): Observable<Reading>{
        let body = JSON.stringify({ task });
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/tasks/' + TOPIC_ID;

        return this.http.post(url, body, options)
            .map(response => response.json());
    }

    addListenigTask(task: Listening, TOPIC_ID:string): Observable<Listening>{
        let body = JSON.stringify({ task });
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/tasks/' + TOPIC_ID;

        return this.http.post(url, body, options)
            .map(response => response.json());
    }
    
    addQuestionForLGT(questions: Array<Question>, TOPIC_ID:string): Observable<Array<Question>>{
        let body = JSON.stringify({ questions });
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/tasks/' + TOPIC_ID;

        return this.http.post(url, body, options)
            .map(response => response.json());
    }
    
    addQuestionForSpeaking(questions: Array<Question>, TOPIC_ID:string): Observable<Array<Question>>{
        let body = JSON.stringify({ questions });
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/tasks/' + TOPIC_ID;

        return this.http.post(url, body, options)
            .map(response => response.json());
    }
}