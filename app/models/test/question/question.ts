import { Answer } from "../answer/answer";

export class Question {
	level: string;
	description: string;
	answersId: Array<Answer>;
	title: string;

	constructor(n: number) {
		this.level = 'level';
		this.description = '';
		this.title = '';
		this.answersId = [];
		for(var i=0; i< n; i++){
			this.answersId.push(new Answer);
		}
	}
}
