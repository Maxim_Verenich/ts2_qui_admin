import {Task} from "../task/task";
import {Text} from "../text/text";

export class Reading{
    text : any;
    translationTask : any;
    statementTask: any;
    
    constructor(){
        this.text = new Text();
        this.translationTask = new Task(1, 3);
        this.statementTask = new Task(1, 2);
    }
}
