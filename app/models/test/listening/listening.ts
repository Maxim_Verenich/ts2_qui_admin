import {Task} from "../task/task";
import {Text} from "../text/text";

export class Listening {
	text : any;
    questionTask:any;
    completeTheSentencesTask:any;
    level:string;

    constructor() {
   		this.text = new Text();
        this.questionTask = new Task(1, 0);
        this.completeTheSentencesTask = new Task(1, 3);
        this.level='level';
    }
}