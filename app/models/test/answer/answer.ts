export class Answer {
	text: string;
	isCorrect: boolean;

	constructor() {
		this.text = '';
		this.isCorrect = false;
	}
}