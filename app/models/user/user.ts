export class User {
    _id:string;
    firstName:string;
    lastName:string;
    role:number;
    mail:string;
    photo:string;

    getAllInfo() {
        return this.firstName + this.lastName + this.mail;
    }
}