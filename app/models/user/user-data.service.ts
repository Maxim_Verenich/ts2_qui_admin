import {Injectable, OnInit} from '@angular/core';
import {User} from "./user";
import {Test} from "../test/test";

import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable }     from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import {CoolLocalStorage} from "angular2-cool-storage/index";
import {Constants} from "../../consts";

@Injectable()
export class UserDataService implements OnInit{
    localStorage: CoolLocalStorage;

    constructor(localStorage: CoolLocalStorage,
                private http: Http) {
        this.localStorage = localStorage;
    }

    ngOnInit() {
        this.localStorage.setItem('itemKey', 'itemValue');

        console.log(this.localStorage.getItem('itemKey'));

        this.localStorage.setObject('itemKey', {
            someObject: 3
        });

        console.log(this.localStorage.getObject('itemKey'));
    }

    getUsersList (role:number): Observable<User[]> {
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        let url = Constants.BASE_URL + '/users/role/' + role;
        return this.http.get(url, options)
            .map(response => response.json());
    }

    getUserById(id: string): Observable<User> {
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken});
        let options = new RequestOptions({ headers: headers});
        let url = Constants.BASE_URL + '/users/' + id;
        console.log('get user' + id);
        return this.http.get(url, options)
            .map(response => response.json())
    }

    assignTest(test: Object) : Observable<Test> {
        let body = JSON.stringify({ test});
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/tests/';
        return this.http.post(url, body, options)
            .map(response => response.json());
    }

    addGuest(user: User) : Observable<User>{
        let body = JSON.stringify({ user });
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/users/';
        return this.http.post(url, body, options)
            .map(response => response.json());
    }

    sendTestsForTeacher(testsId, teacher:User): Observable<any>{
        let body = JSON.stringify({testsId});
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        console.log(body);
        let url = Constants.BASE_URL + '/tests/reviewerId/' + teacher._id;
        return this.http.post(url, body, options);
    }
}