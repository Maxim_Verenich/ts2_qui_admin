import {Injectable} from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable }     from 'rxjs/Observable'
import {Test} from "./test";
import 'rxjs/add/operator/toPromise';
import {Constants} from "../../consts";

@Injectable()
export class TestsService{

    constructor(private http: Http) {
    }
    

    getTestsList(userId:string): Observable<Test[]>{
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken});
        let options = new RequestOptions({ headers: headers });
        var url = Constants.BASE_URL + '/tests/' + userId + '/all';
        console.log('url ', url);
        return this.http.get(url, options)
            .map(response => response.json());

    }
    
    getTestById(testId: string): Observable<Test> {
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken});
        let options = new RequestOptions({ headers: headers });
        var url = Constants.BASE_URL + '/userAnswers/' + testId + '/statistics';
        console.log('url ', url);
        return this.http.get(url, options)
            .map(response => response.json());
    }
    
    getSiteStatistic(): Observable<any>{
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken, 'Content-Type' : 'application/json'});
        let options = new RequestOptions({ headers: headers });
        let url = Constants.BASE_URL + '/statistic/amounts';
        return this.http.get(url, options)
            .map(response => response.json());
    }
}