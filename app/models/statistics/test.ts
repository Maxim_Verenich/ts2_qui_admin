export class Test {
    part1:Array<number>;
    part2:Array<number>;
    part3:Array<number>;
    part4:Array<number>;
    marks:Array<number>;
    isChecked:boolean;

    constructor(obj){
       console.log("obj", obj);
        this.isChecked = true;
        this.part1 = obj.lexicalGrammar.map(answer => answer.isCorrect).slice(0, -1) || [0];
        this.part2 = obj.reading.map(answer => answer.isCorrect).slice(0, -1) || [0];
        this.part3 = obj.listening.map(answer => answer.isCorrect).slice(0, -1) || [0];
        this.part4 = new Array(this.part1.length);
        var arr = obj.speaking.map(answer => answer.isCorrect).slice(0, -1) || [0];
        for(var i = 0; i < this.part1.length; ++i){
            this.part4[i] = 0;
        }
        for(var i = 0; i < arr.length; ++i){
            this.part4[i] = arr[i];
        }
        this.marks = [obj.lexicalGrammar.slice(-1), obj.reading.slice(-1), obj.listening.slice(-1), obj.speaking.slice(-1)];
    }
}