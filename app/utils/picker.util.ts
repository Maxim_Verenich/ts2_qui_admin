import {Injectable} from '@angular/core';

declare function MaterialDatePicker(options: any): void;

@Injectable()
export class PickerUtil{
    openPicker(event, obj, attr, options = {}) : any{
        
        var element = event.target;
        
        const picker = new MaterialDatePicker(options)
            .on('submit', (d) => {
                element.value = d._d;
                obj[attr] = +d._d;
            });
            picker.open();
    }
}