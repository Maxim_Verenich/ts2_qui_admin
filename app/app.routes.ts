import {provideRouter, RouterConfig} from '@angular/router';

import {Authorization} from './components/authorization/authorization';
import {Home} from "./components/admin-home/admin-home";
import {UsersList} from "./components/admin-home/users/usersList";
import {TeachersList} from "./components/admin-home/teachers/teachersList";
import {Statistics} from "./components/admin-home/statistics/statistics";
import {Test} from "./components/admin-home/test/test";
import {UserChartComponent} from "./components/charts/user-charts/user-charts";
import {TeacherDetailsComponent} from "./components/admin-home/teachers/teacher-details/teacher-details";
import {LoggedInGuard} from "./components/authorization/logged-in.guard";


//noinspection TypeScriptValidateTypes
export const routes:RouterConfig = [
    {path: '', component: Authorization},
    {path: 'home', component: Home, canActivate: [LoggedInGuard]},
    {path: 'users', component: UsersList, canActivate: [LoggedInGuard] },
    {path: 'teachers', component: TeachersList, canActivate: [LoggedInGuard] },
    {path: 'statistics', component: Statistics, canActivate: [LoggedInGuard] },
    {path: 'test', component: Test, canActivate: [LoggedInGuard] },
    // {path: 'userDetail/0', component: UserChartComponent, canActivate: [LoggedInGuard] },
    {path: 'teacherDetail/:id', component: TeacherDetailsComponent, canActivate: [LoggedInGuard] },
    {path: 'userDetail/:userId/:id', component: UserChartComponent, canActivate: [LoggedInGuard] }
]

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];
