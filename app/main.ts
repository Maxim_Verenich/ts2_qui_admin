import { bootstrap }    from '@angular/platform-browser-dynamic';
import { APP_ROUTER_PROVIDERS } from './app.routes';
import { AppComponent } from './app.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { COOL_STORAGE_PROVIDERS } from 'angular2-cool-storage';
import {LoggedInGuard} from "./components/authorization/logged-in.guard";
import {UserService} from "./components/authorization/user.service";
import {UserDataService} from "./models/user/user-data.service";
import {TestDataService} from "./models/test/test-data.service";
import {HTTP_PROVIDERS} from "@angular/http";
import {TestsService} from "./models/statistics/test-data.service";
import {PickerUtil} from "./utils/picker.util";
import {NotifyService} from "./models/notify/notify.service";

//noinspection TypeScriptValidateTypes
bootstrap(AppComponent, [COOL_STORAGE_PROVIDERS, UserService, LoggedInGuard, 
    UserDataService, TestDataService, TestsService, HTTP_PROVIDERS,
    APP_ROUTER_PROVIDERS, PickerUtil, NotifyService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
])
    .catch(err => console.error(err));
   