export class Constants {

	public static READING_ID: string = '578e1ac0364a9658259c61d6';
	public static LEXICAL_GRAMMAR_ID: string = '578e1ab7364a9658259c61d5';
	public static LISTENING_ID: string = '578e1ac7364a9658259c61d7';
	public static SPEAKING_ID: string = '578e1acd364a9658259c61d8';
	public static LEVELS: Array<String> = ['A1', 'A2', 'B1', 'B2', 'C1'];
	public static USER_ROLE: string = '0';
	public static TEACHER_ROLE: string = '1';
	public static ADMIN_ROLE: string = '2';

	// http://localhost:8083/api
	// 'https://gentle-woodland-30415.herokuapp.com/api'
	public static BASE_URL: string = 'http://localhost:8083/api';
}

